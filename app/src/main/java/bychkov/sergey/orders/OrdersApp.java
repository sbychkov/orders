package bychkov.sergey.orders;

import android.app.Application;

import com.squareup.otto.Bus;

import bychkov.sergey.orders.util.EventBus;
import bychkov.sergey.orders.util.HelperFactory;

/**
 * Created by serge on 17.12.2014.
 */
public class OrdersApp extends Application{

    private Bus bus;

    public OrdersApp() {
        super();
    }



    /**
     * Called when the application is starting, before any activity, service,
     * or receiver objects (excluding content providers) have been created.
     * Implementations should be as quick as possible (for example using
     * lazy initialization of state) since the time spent in this function
     * directly impacts the performance of starting the first activity,
     * service, or receiver in a process.
     * If you override this method, be sure to call super.onCreate().
     */

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());

        bus = EventBus.getInstance();
        /*try {
            HelperFactory.getHelper().getClientDao().createIfNotExists(
                    new Client(1,"TestClient",null,null,false));
            HelperFactory.getHelper().getItemDao().createIfNotExists(
                    new Item(1,"TestItem",null,null,"шт.",false));
            HelperFactory.getHelper().getPriceDao().createIfNotExists(
                    new PriceList(1,HelperFactory.getHelper().getItemDao().queryForId(1L), BigDecimal.ONE,1));
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }

    /**
     * This method is for use in emulated process environments.  It will
     * never be called on a production Android device, where processes are
     * removed by simply killing them; no user code (including this callback)
     * is executed when doing so.
     */
    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }

}
