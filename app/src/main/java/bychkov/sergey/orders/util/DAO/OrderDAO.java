package bychkov.sergey.orders.util.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import bychkov.sergey.orders.model.Order;

/**
 * Created by serge on 18.12.2014.
 */
public class OrderDAO  extends BaseDaoImpl<Order,Long> {
    /**
     * Construct our base DAO class. The dataClass provided must have its fields marked with {@link DatabaseField} or
     * javax.persistance annotations.
     *
     * @param connectionSource Source of our database connections.
     * @param dataClass
     */
    public OrderDAO(ConnectionSource connectionSource, Class<Order> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
    public List<Order> getAll() throws SQLException{
        return this.queryForAll();
    }
    public List<Order> getUnRegistered() throws SQLException {
              return  this.queryBuilder().where().isNull("regId").query();
            }
}
