package bychkov.sergey.orders.util;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;
import com.squareup.otto.DeadEvent;


/**
 * Created by serge on 07.04.2015.
 */
public class EventBus {
    private static Bus ourInstance = new MyBus();

    public static Bus getInstance() {
        return ourInstance;
    }

    private EventBus() {
    }


    private static class MyBus extends Bus {

        /**
         * Posts an event to all registered handlers.  This method will return successfully after the event has been posted to
         * all handlers, and regardless of any exceptions thrown by handlers.
         * <p/>
         * <p>If no handlers have been subscribed for {@code event}'s class, and {@code event} is not already a
         * {@link DeadEvent}, it will be wrapped in a DeadEvent and reposted.
         *
         * @param event event to post.
         * @throws NullPointerException if the event is null.
         */
        @Override
        public void post(final Object event) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                super.post(event);
            }else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        MyBus.super.post(event);
                    }
                });
            }
        }
    }

}