package bychkov.sergey.orders.util.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.util.HelperFactory;

/**
 * Created by serge on 02.02.2015.
 */
public class OrdersAdapter extends BaseAdapter {

    private List<Order> objects = new ArrayList<>();


    private Context context;
    private LayoutInflater layoutInflater;

    public OrdersAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        try {
            objects = HelperFactory.getHelper().getOrderDao().getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */


    @Override
    public int getCount() {
        return objects.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Order order = objects.get(position);

        if (view == null) {
            view = layoutInflater.inflate(R.layout.order_layout, parent, false);
        }
        final TextView clientView = (TextView) view.findViewById(R.id.clientName);
        clientView.setText(order.getClient().getName());

        final TextView dateView = (TextView) view.findViewById(R.id.date);
        dateView.setText(date2string(order.getCreateDate()));

        final TextView sumView = (TextView) view.findViewById(R.id.sum);
        if (order.getSum() != null) {
            sumView.setText(order.getSum().toPlainString());
        } else {
            sumView.setText("0");
        }
        if (order.getRegId() != null) {
            view.setBackgroundColor(Color.argb(10, 0, 50, 0));

        } else {
            view.setBackgroundColor(Color.argb(10, 0, 0, 50));

        }

        return view;
    }

    private static String date2string(Date date) {
        // SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(date);
    }
}
