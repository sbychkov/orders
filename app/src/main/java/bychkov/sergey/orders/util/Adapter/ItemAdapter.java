package bychkov.sergey.orders.util.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.util.HelperFactory;

@Deprecated
public class ItemAdapter extends BaseAdapter {

    private List<Item> objects = new ArrayList<>();


    private Context context;
    private LayoutInflater layoutInflater;
    private Item parent;

    public ItemAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        try {
            objects.addAll(HelperFactory.getHelper().getItemDao().getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Item getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Item i = objects.get(position);

        if (view == null) {
            view = layoutInflater.inflate(R.layout.item, parent, false);
        }
        final TextView nameView = (TextView) view.findViewById(R.id.price);
        if (i.isCategory()) {
            nameView.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_menu_add, 0, 0, 0);
        }
        nameView.setText(i.getName());
        return view;
    }

    public void setParent(Item parent){
        this.parent=parent;
        try {
            List<Item> allItems = HelperFactory.getHelper().getItemDao().getByParent(parent);
            if(allItems!=null) objects = allItems;
            this.notifyDataSetChanged();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}
