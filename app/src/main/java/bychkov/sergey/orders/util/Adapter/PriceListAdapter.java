package bychkov.sergey.orders.util.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.util.HelperFactory;

public class PriceListAdapter extends BaseAdapter {

    private List<Item> objects = new ArrayList<>();
    private List<Item> roots = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public PriceListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        try {
            objects.addAll(HelperFactory.getHelper().getItemDao().getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PriceListAdapter(Context context, Item parent) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        try {
            objects.addAll(HelperFactory.getHelper().getItemDao().getByParent(parent));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Item getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Item item = objects.get(position);

        if (view == null) {
            view = layoutInflater.inflate(R.layout.pricelist, parent, false);
        }

        if (roots.contains(item)) {
            view = layoutInflater.inflate(R.layout.pricelist_category, parent, false);
            view.setBackgroundColor(Color.LTGRAY);
        } else {
            view = layoutInflater.inflate(R.layout.pricelist, parent, false);
            view.setBackgroundColor(Color.TRANSPARENT);
        }
        TextView nameView = (TextView) view.findViewById(R.id.name);
        if (item.isCategory()){
            nameView.setText(item.getName().toUpperCase());
        }else {
            nameView.setText(item.getName());
        }
            TextView priceView = (TextView) view.findViewById(R.id.price);
        TextView countView = (TextView) view.findViewById(R.id.count);
        if (!item.isCategory()) {
            priceView.setText(item.getPrice().toPlainString() + " руб.");
            countView.setText(String.valueOf(item.getQuantity()) + " " + item.getUnit());
        } else {
            priceView.setText("");
            countView.setText("");
        }
        return view;
    }

    public void setParent(Item parent) {
        try {
            parent = checkRoot(parent);
            objects.clear();
            objects.addAll(roots);
            objects.addAll(HelperFactory.getHelper().getItemDao().getByParent(parent));
            notifyDataSetChanged();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Item checkRoot(Item item) {
        if (roots.contains(item)) {
            roots = roots.subList(0, roots.indexOf(item));
            item = item.getParent();
        } else {
            roots.add(item);
        }
        return item;
    }

    public boolean back(){
        int idx = roots.size() - 1;
        if (idx >=0){
            setParent(roots.get(idx));
            return false;
        }else{
            return true;
        }
    }

}
