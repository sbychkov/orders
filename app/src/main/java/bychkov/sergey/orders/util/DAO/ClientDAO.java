package bychkov.sergey.orders.util.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import bychkov.sergey.orders.model.Client;

/**
 * Created by serge on 17.12.2014.
 */
public class ClientDAO extends BaseDaoImpl<Client,Long>{
    /**
     * Construct our base DAO class. The dataClass provided must have its fields marked with {@link com.j256.ormlite.field.DatabaseField} or
     * javax.persistance annotations.
     *
     * @param connectionSource Source of our database connections.
     * @param dataClass
     */
    public ClientDAO(ConnectionSource connectionSource, Class<Client> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Client> getAll() throws SQLException{
        return this.queryForAll();
    }
    public List<Client> getByParent(Client parent ) throws SQLException {
        if (parent!= null) {
            return queryForEq("parent_id", parent);
        }else{
            return queryBuilder().where().isNull("parent_id").query();
        }
    }
    public List<Client> getByCode(Integer code ) throws SQLException {
        return queryForEq("code",code);
    }
    public List<Client> findIsCategoryByCode(Integer code) throws SQLException {
        return queryBuilder().where().eq("code", code).and().eq("category",true).query();
    }

    public List<Client> findIsNotCategoryByCode(Integer code) throws SQLException {
        return queryBuilder().where().eq("code", code).and().eq("category",false).query();
    }
}
