package bychkov.sergey.orders.util.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import bychkov.sergey.orders.model.Item;

/**
 * Created by serge on 17.12.2014.
 */
public class ItemDAO extends BaseDaoImpl<Item, Long> {

    /**
     * Construct our base DAO class. The dataClass provided must have its fields marked with {@link DatabaseField} or
     * javax.persistance annotations.
     *
     * @param connectionSource Source of our database connections.
     * @param dataClass
     */
    public ItemDAO(ConnectionSource connectionSource, Class<Item> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Item> getAll() throws SQLException {
        return this.queryForAll();
    }

    public List<Item> getByParent(Item parent) throws SQLException {
        if (parent != null) {
            return queryForEq("parent_id", parent);
        } else {
            return queryBuilder().where().isNull("parent_id").query();
        }
    }

    public List<Item> getByCode(Integer code) throws SQLException {
        return queryForEq("code", code);
    }

    public List<Item> findIsCategoryByCode(Integer code) throws SQLException {
        return queryBuilder().where().eq("code", code).and().eq("category", true).query();
    }

    public List<Item> findIsNotCategoryByCode(Integer code) throws SQLException {
        return queryBuilder().where().eq("code", code).and().eq("category", false).query();
    }

    public List<Item> findByCategoryAndCode(Integer code, boolean cat) throws SQLException {
        return queryBuilder().where().eq("code", code).and().eq("category", cat).query();
    }
}
