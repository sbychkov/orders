package bychkov.sergey.orders.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.model.OrderElement;
import bychkov.sergey.orders.util.DAO.ClientDAO;
import bychkov.sergey.orders.util.DAO.ItemDAO;
import bychkov.sergey.orders.util.DAO.OrderDAO;
import bychkov.sergey.orders.util.DAO.OrderElementDAO;

/**
 * Created by serge on 17.12.2014.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper{


    private static final String TAG = DatabaseHelper.class.getSimpleName();

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final String DATABASE_NAME ="orders.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DATABASE_VERSION = 3;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private ClientDAO clientDao = null;
    private ItemDAO itemDao = null;
//    private PriceDAO priceDao = null;
    private OrderDAO orderDao = null;
    private OrderElementDAO orderElementDao=null;

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }



    /**
     * What to do when your database needs to be created. Usually this entails creating the tables and loading any
     * initial data.
     * <p/>
     * <p>
     * <b>NOTE:</b> You should use the connectionSource argument that is passed into this method call or the one
     * returned by getConnectionSource(). If you use your own, a recursive call or other unexpected results may result.
     * </p>
     *
     * @param db         Database being created.
     * @param connectionSource
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try
        {
            TableUtils.createTable(connectionSource, Item.class);
            TableUtils.createTable(connectionSource, Client.class);
  //          TableUtils.createTable(connectionSource, PriceList.class);
            TableUtils.createTable(connectionSource, Order.class);
            TableUtils.createTable(connectionSource, OrderElement.class);
        }
        catch (SQLException e){
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    /**
     * What to do when your database needs to be updated. This could mean careful migration of old data to new data.
     * Maybe adding or deleting database columns, etc..
     * <p/>
     * <p>
     * <b>NOTE:</b> You should use the connectionSource argument that is passed into this method call or the one
     * returned by getConnectionSource(). If you use your own, a recursive call or other unexpected results may result.
     * </p>
     *
     * @param db         Database being upgraded.
     * @param connectionSource To use get connections to the database to be updated.
     * @param oldVersion       The version of the current database so we can know what to do to the database.
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable(connectionSource, Item.class, true);
//            TableUtils.dropTable(connectionSource, PriceList.class, true);
            TableUtils.dropTable(connectionSource, Client.class, true);
            TableUtils.dropTable(connectionSource, Order.class, true);
            TableUtils.dropTable(connectionSource, OrderElement.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e){
            Log.e(TAG,"error upgrading db "+DATABASE_NAME+"from ver "+oldVersion);
            throw new RuntimeException(e);
        }
    }

    public void clean(){
        onUpgrade(getWritableDatabase(),getConnectionSource(),DATABASE_VERSION,DATABASE_VERSION);
    }

  /*  public PriceDAO getPriceDao() throws SQLException {
        if (priceDao==null){
            priceDao= new PriceDAO(getConnectionSource(),PriceList.class);
        }
        return priceDao;
    }
*/
    public ClientDAO getClientDao() throws SQLException {
        if (clientDao==null){
            clientDao= new ClientDAO(getConnectionSource(),Client.class);
        }
        return clientDao;
    }

    public ItemDAO getItemDao() throws SQLException {
        if(itemDao==null){
            itemDao= new ItemDAO(getConnectionSource(),Item.class);
        }
        return itemDao;
    }

    public OrderDAO getOrderDao() throws SQLException {
        if(orderDao==null){
            orderDao= new OrderDAO(getConnectionSource(),Order.class);
        }
        return orderDao;
    }

    public OrderElementDAO getOrderElementDao() throws SQLException {
        if(orderElementDao==null){
            orderElementDao= new OrderElementDAO(getConnectionSource(),OrderElement.class);
        }
        return orderElementDao;
    }

}
