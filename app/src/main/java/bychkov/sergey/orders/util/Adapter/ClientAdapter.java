package bychkov.sergey.orders.util.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.util.HelperFactory;

public class ClientAdapter extends BaseAdapter {

    private static final String TAG = ClientAdapter.class.getSimpleName();

    private List<Client> objects = new ArrayList<>();
    private List<Client> roots = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;


    public ClientAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        try {
            List<Client> allClient = HelperFactory.getHelper().getClientDao().getAll();
            if (allClient != null) {
                objects.addAll(allClient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ClientAdapter(Context context, Client parent) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        try {
            List<Client> allClient = HelperFactory.getHelper().getClientDao().getByParent(parent);
            if (allClient != null) {
                objects.addAll(allClient);
            }
        } catch (SQLException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Client getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Client c = objects.get(position);

        if (view == null) {
            view = layoutInflater.inflate(R.layout.client, parent, false);
        }
        final TextView nameView = (TextView) view.findViewById(R.id.price);
        if (c.isCategory()){
            nameView.setText(c.getName().toUpperCase());
        }else {
            nameView.setText(c.getName());
        }
            if (roots.contains(c)) {
            view.setBackgroundColor(Color.DKGRAY);
            //nameView.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_menu_forward,0,0,0);
        } else {
            view.setBackgroundColor(Color.TRANSPARENT);
            if (c.isCategory()) {
                // Adding icon
                // nameView.setCompoundDrawablesWithIntrinsicBounds(0,0,android.R.drawable.ic_menu_back,  0);
            }
        }
        return view;
    }

    public void setParent(Client parent) {
        try {
            parent = checkRoot(parent);
            objects.clear();
            objects.addAll(roots);
            objects.addAll(HelperFactory.getHelper().getClientDao().getByParent(parent));
            notifyDataSetChanged();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Client checkRoot(Client item) {
        if (roots.contains(item)) {
            roots = roots.subList(0, roots.indexOf(item));
            item = item.getParent();
        } else {
            roots.add(item);
        }
        return item;
    }

    public boolean back() {
        int idx = roots.size() - 1;
        if (idx >= 0) {
            setParent(roots.get(idx));
            return false;
        } else {
            return true;
        }
    }

}
