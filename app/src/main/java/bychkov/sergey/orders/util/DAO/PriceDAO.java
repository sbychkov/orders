package bychkov.sergey.orders.util.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import bychkov.sergey.orders.model.PriceList;

/**
 * Created by serge on 17.12.2014.
 */
public class PriceDAO extends BaseDaoImpl<PriceList,Long> {

    /**
     * Construct our base DAO class. The dataClass provided must have its fields marked with {@link com.j256.ormlite.field.DatabaseField} or
     * javax.persistance annotations.
     *
     * @param connectionSource Source of our database connections.
     * @param dataClass
     */
    public PriceDAO(ConnectionSource connectionSource, Class<PriceList> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<PriceList> getAllPrice() throws SQLException{
        return this.queryForAll();
    }

}
