package bychkov.sergey.orders.util.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.model.OrderElement;

/**
 * Created by serge on 18.12.2014.
 */
public class OrderElementDAO  extends BaseDaoImpl<OrderElement,Long> {
    /**
     * Construct our base DAO class. The dataClass provided must have its fields marked with {@link com.j256.ormlite.field.DatabaseField} or
     * javax.persistence annotations.
     *
     * @param connectionSource Source of our database connections.
     * @param dataClass
     */
    public OrderElementDAO(ConnectionSource connectionSource, Class<OrderElement> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
    public List<OrderElement> getAllElement() throws SQLException{
        return this.queryForAll();
    }
    public List<OrderElement> getElementByOrder(Order order) throws SQLException {
    return this.queryBuilder().where().eq("order",order).query();
    }
}
