package bychkov.sergey.orders.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by serge on 17.12.2014.
 */
@Deprecated
@DatabaseTable
public class PriceList implements Serializable{
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(foreign = true)
    private Item item;
    @DatabaseField
    private BigDecimal price;
    @DatabaseField
    private int quantity;

    public PriceList() {
    }

    public PriceList(long id, Item item, BigDecimal price, int quantity) {
        this.id = id;
        this.item = item;
        this.price = price;
        this.quantity = quantity;
    }

    public PriceList(long id) {
        this.id = id;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "PriceList{" +
                "id=" + id +
                ", item=" + item +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriceList priceList = (PriceList) o;

        if (quantity != priceList.quantity) return false;
        if (!item.equals(priceList.item)) return false;
        if (price != null ? !price.equals(priceList.price) : priceList.price != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + item.hashCode();
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + quantity;
        return result;
    }
}
