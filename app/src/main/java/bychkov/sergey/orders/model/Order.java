package bychkov.sergey.orders.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by serge on 18.12.2014.
 */
@DatabaseTable
public class Order {
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private Long regId;
    @DatabaseField(foreign = true,foreignColumnName = "id")
    private Client client;
    @DatabaseField
    private Date createDate;
    @DatabaseField
    private BigDecimal sum;
    @ForeignCollectionField(foreignFieldName = "order")
    private Collection<OrderElement> elements;


    public Order() {
    }

    public Order(Date createDate, BigDecimal sum, Collection<OrderElement> elements) {
        this.createDate = createDate;
        this.sum = sum;
        this.elements = elements;
    }

    public Order(Long id, Date createDate, BigDecimal sum, Collection<OrderElement> elements) {
        this.id = id;
        this.createDate = createDate;
        this.sum = sum;
        this.elements = elements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Collection<OrderElement> getElements() {
        return elements;
    }

    public void setElements(Collection<OrderElement> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", sum=" + sum +
                ", elements=" + elements +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (createDate != null ? !createDate.equals(order.createDate) : order.createDate != null)
            return false;
        if (elements != null ? !elements.equals(order.elements) : order.elements != null)
            return false;
        if (sum != null ? !sum.equals(order.sum) : order.sum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (sum != null ? sum.hashCode() : 0);
        result = 31 * result + (elements != null ? elements.hashCode() : 0);
        return result;
    }

    public Long getRegId() {
        return regId;
    }

    public void setRegId(Long regId) {
        this.regId = regId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

// for alternative put order
    public Map<String,String> toMap(){
        Map<String,String> result = new HashMap<String,String>();

        result.put("Client",client.getCode().toString());
        result.put("CreateDate",createDate.toString());

        for(OrderElement oe:elements){
            result.put(oe.getItem().getCode().toString(),oe.getQuantity().toString());
        }

        return result;
    }

}
