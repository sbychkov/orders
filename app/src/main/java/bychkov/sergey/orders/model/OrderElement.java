package bychkov.sergey.orders.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by serge on 18.12.2014.
 */
@DatabaseTable
public class OrderElement implements Serializable{
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(foreign = true,foreignColumnName = "id")
    private Order order;
    @DatabaseField(foreign = true,foreignColumnName = "id")
    private Item item;
    @DatabaseField
    private BigDecimal price;
    @DatabaseField
    private Integer quantity;

    public OrderElement(Order order, Item item, BigDecimal price, Integer quantity) {
        this.order = order;
        this.item = item;
        this.price = price;
        this.quantity = quantity;
    }

    public OrderElement() {
    }



    public OrderElement(Long id, Order order, Item item, BigDecimal price, Integer quantity) {
        this.id = id;
        this.order = order;
        this.item = item;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
