package bychkov.sergey.orders.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by serge on 17.12.2014.
 */
@DatabaseTable
public class Client implements Serializable{
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private String name;
    @DatabaseField
    private Integer code;
    @ForeignCollectionField
    private Collection<Client> children;
    @DatabaseField(foreignColumnName = "id",foreign = true)
    private Client parent;
    @DatabaseField
    private boolean category;


    public Client() {

    }

    public Client(long id) {
        this.id = id;
    }

    public Client(long id, String name, Integer code, Client parent, boolean category) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.parent = parent;
        this.category = category;
    }

    public Client(boolean category, long id, String name, Integer code, List children, Client parent) {
        this.category = category;
        this.id = id;
        this.name = name;
        this.code = code;
        this.children = children;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Collection<Client> getChildren() {  return children;    }

    public void setChildren(Collection<Client> children) {
        this.children = children;
    }

    public Client getParent() {
        return parent;
    }

    public void setParent(Client parent) {
        this.parent = parent;
    }

    public boolean isCategory() {
        return category;
    }

    public void setCategory(boolean category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", children=" + children +
                ", parent=" + parent +
                ", category=" + category +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (category != client.category) return false;
        if (children != null ? !children.equals(client.children) : client.children != null)
            return false;
        if (code != null ? !code.equals(client.code) : client.code != null) return false;
        if (!name.equals(client.name)) return false;
        if (parent != null ? !parent.equals(client.parent) : client.parent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (children != null ? children.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (category ? 1 : 0);
        return result;
    }
}
