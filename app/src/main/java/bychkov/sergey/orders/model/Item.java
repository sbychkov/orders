package bychkov.sergey.orders.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Created by serge on 17.12.2014.
 */
@DatabaseTable
public class Item implements Serializable{

private static final String LOG_TAG = Item.class.getSimpleName();
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private String name;
    @DatabaseField
    private boolean category;
    @ForeignCollectionField
    private Collection<Item> children;
    @DatabaseField
    private String unit;
    @DatabaseField(foreignColumnName = "id",foreign = true)
    private Item parent;
    @DatabaseField
    private Integer code;
    @DatabaseField(defaultValue = "0")
    private BigDecimal price;
    @DatabaseField(defaultValue = "0")
    private int quantity;

    public Item(long id) {
        this.id = id;
    }

    public Item(){
    }

    public Item(String name, boolean category, Collection<Item> children, String unit, Item parent, Integer code, BigDecimal price, int quantity, Long id) {
        this.name = name;
        this.category = category;
        this.children = children;
        this.unit = unit;
        this.parent = parent;
        this.code = code;
        this.price = price;
        this.quantity = quantity;
        this.id = id;
    }

    public Item(String name, boolean category, Collection<Item> children, String unit, Item parent,Integer code, BigDecimal price, int quantity) {
        this.name = name;
        this.category = category;
        this.children = children;
        this.unit = unit;
        this.parent = parent;
        this.code = code;
        this.price = price;
        this.quantity = quantity;
    }

    public Item(String name, boolean category, String unit, Integer code, BigDecimal price, int quantity) {
        this.name = name;
        this.category = category;
        this.unit = unit;
        this.code = code;
        this.price = price;
        this.quantity = quantity;
    }

    public Item(String name, boolean category, String unit, Item parent, Integer code) {
        this.name = name;
        this.category = category;
        this.unit = unit;
        this.parent = parent;
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCategory() {
        return category;
    }

    public void setCategory(boolean category) {
        this.category = category;
    }

    public Collection<Item> getChildren() {
        return children;
    }

    public void setChildren(Collection<Item> children) {
        this.children = children;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Item getParent() {
        return parent;
    }

    public void setParent(Item parent) {
        this.parent = parent;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (category != item.category) return false;
        if (quantity != item.quantity) return false;
        if (children != null ? !children.equals(item.children) : item.children != null)
            return false;
        if (code != null ? !code.equals(item.code) : item.code != null) return false;
        if (name != null ? !name.equals(item.name) : item.name != null) return false;
        if (parent != null ? !parent.equals(item.parent) : item.parent != null) return false;
        if (price != null ? !price.equals(item.price) : item.price != null) return false;
        if (unit != null ? !unit.equals(item.unit) : item.unit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (category ? 1 : 0);
        result = 31 * result + (children != null ? children.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + quantity;
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category=" + category +
                ", children=" + children +
                ", unit='" + unit + '\'' +
                ", parent=" + parent +
                ", code='" + code + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }


}
