package bychkov.sergey.orders.event;

/**
 * Created by serge on 03.06.2015.
 */
public class BackEvent {
    public BackEvent(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public enum Type {backButton, exitApp}

    private final Type type;
}
