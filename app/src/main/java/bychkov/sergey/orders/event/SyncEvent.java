package bychkov.sergey.orders.event;

/**
 * Created by serge on 08.04.2015.
 */
public class SyncEvent {

    private final Type type;
    private Integer value;
    private String title;


    public SyncEvent(Type type, String title) {
        this.title = title;
        this.type = type;
    }

    public SyncEvent(Type type, Integer value) {
        this.value = value;
        this.type = type;
    }

    public SyncEvent(Type type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Type getType() {
        return type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public enum Type {Start, Stop, MaxValue, MaxTotalValue, IncTotal, IncValue, Inc, Title, TotalTitle}
}

