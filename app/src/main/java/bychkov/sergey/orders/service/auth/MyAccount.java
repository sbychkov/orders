package bychkov.sergey.orders.service.auth;

import android.accounts.Account;
import android.os.Parcel;

/**
 * Created by serge on 03.04.2015.
 */
public class MyAccount extends Account {

    public static final String TYPE = "bychkov.sergey.orders.account";

    public static final String TOKEN_FULL_ACCESS = "bychkov.sergey.orders.TOKEN_FULL_ACCESS";

    public static final String KEY_PASSWORD = "bychkov.sergey.orders.KEY_PASSWORD";

    public MyAccount(Parcel in) {
        super(in);
    }

    public MyAccount(String name) {
        super(name, TYPE);
    }

}