package bychkov.sergey.orders.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class SyncServiceOld extends Service {
    // constant
    private Sync sync;
    public static final long INTERVAL = 10 * 60 * 1000; // 10 minutes

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        if (sync==null){
            sync=new Sync(getApplicationContext());
        }
        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0,INTERVAL);
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            /*mHandler.post(new Runnable() {

                @Override
                public void run() {
                    // display toast
                    sync.execute(new Object[]{"update"});

                    Toast.makeText(getApplicationContext(), getDateTime() + " updated",
                            Toast.LENGTH_SHORT).show();
                }

            });*/
        }

        private String getDateTime() {
            // get date time in custom format
            SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd - HH:mm:ss]");
            return sdf.format(new Date());
        }
    }
}
