package bychkov.sergey.orders.service.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.squareup.otto.Bus;

import java.util.Date;

import bychkov.sergey.orders.event.Message;
import bychkov.sergey.orders.event.SyncEvent;
import bychkov.sergey.orders.util.EventBus;

/**
 * Created by serge on 03.04.2015.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {


    public static final String TAG = "SyncAdapter";
    private ContentResolver mContentResolver;
    private Bus bus = EventBus.getInstance();


    /**
     * Creates an {@link android.content.AbstractThreadedSyncAdapter}.
     *
     * @param context        the {@link android.content.Context} that this is running within.
     * @param autoInitialize if true then sync requests that have
     *                       {@link ContentResolver#SYNC_EXTRAS_INITIALIZE} set will be internally handled by
     *                       {@link android.content.AbstractThreadedSyncAdapter} by calling
     *                       {@link ContentResolver#setIsSyncable(android.accounts.Account, String, int)} with 1 if it
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.mContentResolver = context.getContentResolver();
    }

    /**
     * Creates an {@link AbstractThreadedSyncAdapter}.
     *
     * @param context            the {@link android.content.Context} that this is running within.
     * @param autoInitialize     if true then sync requests that have
     *                           {@link ContentResolver#SYNC_EXTRAS_INITIALIZE} set will be internally handled by
     *                           {@link AbstractThreadedSyncAdapter} by calling
     *                           {@link ContentResolver#setIsSyncable(android.accounts.Account, String, int)} with 1 if it
     *                           is currently set to <0.
     * @param allowParallelSyncs if true then allow syncs for different accounts to run
     *                           at the same time, each in their own thread. This must be consistent with the setting
     *                           in the SyncAdapter's configuration file.
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    /**
     * Creates an {@link android.content.AbstractThreadedSyncAdapter}.
     *
     * @param context            the {@link android.content.Context} that this is running within.
     * @param autoInitialize     if true then sync requests that have
     *                           {@link android.content.ContentResolver#SYNC_EXTRAS_INITIALIZE} set will be internally handled by
     *                           {@link android.content.AbstractThreadedSyncAdapter} by calling
     *                           {@link android.content.ContentResolver#setIsSyncable(android.accounts.Account, String, int)} with 1 if it
     *                           is currently set to <0.
     * @param allowParallelSyncs if true then allow syncs for different accounts to run
     *                           at the same time, each in their own thread. This must be consistent with the setting
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs, ContentResolver mContentResolver) {
        super(context, autoInitialize, allowParallelSyncs);
        this.mContentResolver = mContentResolver;
    }

    /**
     * Perform a sync for this account. SyncAdapter-specific parameters may
     * be specified in extras, which is guaranteed to not be null. Invocations
     * of this method are guaranteed to be serialized.
     *
     * @param account    the account that should be synced
     * @param extras     SyncAdapter-specific parameters
     * @param authority  the authority of this sync request
     * @param provider   a ContentProviderClient that points to the ContentProvider for this
     *                   authority
     * @param syncResult SyncAdapter-specific parameters
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i(TAG, "Beginning network synchronization");

        Updates mUpdate;

        String lastChangeString = getLastChange(account);

        if (lastChangeString != null && !lastChangeString.isEmpty()) {
            mUpdate = new Updates(new Date(Long.getLong(lastChangeString, 0L)),getContext());
        } else {
            mUpdate = new Updates(getContext());
        }

        Date startTime = new Date();
        Boolean full = extras.getBoolean("Full");
        Boolean pricesOnly = extras.getBoolean("pricesOnly");

        bus.post(new Message(null, "Sync started "));

        if ( full) {

            bus.post(new SyncEvent(SyncEvent.Type.MaxTotalValue, 2));

            mUpdate.fetchClients();

            bus.post(new SyncEvent(SyncEvent.Type.Inc));

            mUpdate.fetchItems();

            bus.post(new SyncEvent(SyncEvent.Type.Inc));
            bus.post(new SyncEvent(SyncEvent.Type.Stop));


        } else if (pricesOnly) {
            bus.post(new SyncEvent(SyncEvent.Type.Start));

            mUpdate.updatePrices();

            bus.post(new SyncEvent(SyncEvent.Type.Stop));
        } else {

            bus.post(new SyncEvent(SyncEvent.Type.MaxTotalValue, 4));

            mUpdate.updateOrders();

            bus.post(new SyncEvent(SyncEvent.Type.Inc));

            mUpdate.updateClients();

            bus.post(new SyncEvent(SyncEvent.Type.Inc));

            mUpdate.updateItems();

            bus.post(new SyncEvent(SyncEvent.Type.Inc));

            mUpdate.updatePrices();

            bus.post(new SyncEvent(SyncEvent.Type.Inc));
            bus.post(new SyncEvent(SyncEvent.Type.Stop));
        }

        setLastChange(account, startTime);

        Log.i(TAG, "Network synchronization complete");

        bus.post(new Message(null, "Sync finished"));
    }

    private String getLastChange(Account account) {

        AccountManager accountManager =
                (AccountManager) getContext().getSystemService(Context.ACCOUNT_SERVICE);
        return accountManager.getUserData(account, "lastChange");
    }

    private void setLastChange(Account account, Date time) {
        AccountManager accountManager =
                (AccountManager) getContext().getSystemService(Context.ACCOUNT_SERVICE);
        accountManager.setUserData(account, "lastChange", String.valueOf(time.getTime()));
    }
}


