package bychkov.sergey.orders.service.sync;

import java.util.List;

import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by serge on 23.12.2014.
 */
public interface Api {

    @GET("/client")
    public List<Client> getClients();
    @GET("/item")
    public List<Item> getItems();
    @GET("/price")
    public List<Item> getPrice();
    @GET("/price")
    public List<Item> getPrice(@Query("parent")String parent);
    @POST("/order")
    public String sendOrder(@Body Order order);
    @GET("/order/{id}")
    public Order getOrder(@Path("id") int id);

}
