package bychkov.sergey.orders.service.sync;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Collection;
import java.util.Map;

import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 *
 * @author serge
 */
public interface CtrlApi {

    public static final String PATH = "/";
    public static final String ORDERS_PATH = PATH + "order/";
    public static final String ITEMS_PATH = PATH + "item/";
    public static final String PRICES_PATH = PATH + "price/";
    public static final String CATEGORIES_PATH = PATH + "category/";
    public static final String CLIENTS_PATH = PATH + "client/";

    // Orders
    @POST(ORDERS_PATH)
    public Order addOrder(@Body Order order);

    @GET(ORDERS_PATH)
    public Collection<Order> getOrderList();

    @GET(ORDERS_PATH + "/{id}")
    public Order getOrderById(@Path("id") Long id);

    @PUT(ORDERS_PATH)
    public Order putOrder(@Body Map<String,String> order);

    // Items
    @POST(ITEMS_PATH)
    public Item addItem(@Body Item item);

    @PUT(ITEMS_PATH)
    public Item createItem(@Body Item item);

    @DELETE(ITEMS_PATH)
    public Response deleteItem(@Query("id") Long id);

    @GET(ITEMS_PATH)
    public Collection<Item> getItemList();

    @GET(ITEMS_PATH + "{id}/")
    public Item getItemById(@Path("id") Long id);

    @GET(ITEMS_PATH)
    public Collection<Item> getItemLastChanges(@Query("lastChange") String lastChange);

    @GET(ITEMS_PATH)
    public Item getItemByCode(@Query("code") Integer code);


    // Categories
    @POST(CATEGORIES_PATH)
    public Item addCategory(@Body Item item);

    @PUT(CATEGORIES_PATH)
    public Item createCategory(@Body Item item);

    @DELETE(CATEGORIES_PATH)
    public Response deleteCategory(@Query("id") Long id);

    @GET(CATEGORIES_PATH)
    public Collection<Item> getCategoryList();

    @GET(CATEGORIES_PATH + "{id}/")
    public Item getCategoryById(@Path("id") Long id);

    @GET(CATEGORIES_PATH)
    public Collection<Item> getCategoryLastChanges(@Query("lastChange") String lastChange);

    @GET(CATEGORIES_PATH)
    public Item getCategoryByCode(@Query("code") Integer code);


    // Prices
    @POST(PRICES_PATH)
    public Item setPrice(@Body Item item);

    @DELETE(PRICES_PATH)
    public Response clearPrice(@Query("id") Long id);

    @GET(PRICES_PATH)
    public Collection<Item> getPriceList();

    @GET(PRICES_PATH + "{id}/")
    public Item getPriceById(@Path("id") Long id);

    @GET(PRICES_PATH)
    public Collection<Item> getPriceLastChanges(@Query("lastChange") String lastChange);


    // Clients
    @POST(CLIENTS_PATH)
    public Client addClient(@Body Client client);

    @PUT(CLIENTS_PATH)
    public Client createClient(@Body Client client);

    @DELETE(CLIENTS_PATH)
    public Response deleteClient(@Query("id") Long id);

    @GET(CLIENTS_PATH)
    public Collection<Client> getClientList();

    @GET(CLIENTS_PATH + "{id}/")
    public Client getClientById(@Path("id") Long id);

    @GET(CLIENTS_PATH)
    public Collection<Client> getClientLastChanges(@Query("lastChange") String lastChange);

    @GET(CLIENTS_PATH)
    public Client getClientByCode(@Query("code") Integer code);

    //Auth

}
