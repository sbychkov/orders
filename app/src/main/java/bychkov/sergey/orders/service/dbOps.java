package bychkov.sergey.orders.service;

import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.util.DAO.ClientDAO;
import bychkov.sergey.orders.util.DAO.ItemDAO;
import bychkov.sergey.orders.util.DAO.OrderDAO;
import bychkov.sergey.orders.util.DatabaseHelper;
import bychkov.sergey.orders.util.HelperFactory;

/**
 * Created by serge on 25.02.2015.
 */
public class dbOps {

private static final String TAG = dbOps.class.getSimpleName();




    private static DatabaseHelper getHelper(){
        return HelperFactory.getHelper();
    }

    public static ClientDAO clients(){
        ClientDAO result  =  null;
        try {
            result = getHelper().getClientDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ItemDAO items(){
        ItemDAO result  =  null;
        try {
            result = getHelper().getItemDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static OrderDAO orders(){
        OrderDAO result  =  null;
        try {
            result = getHelper().getOrderDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static List<Order> getOrders(){
        List<Order> result =new ArrayList<>();
        if (orders() != null){
            try {
                result= orders().getAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static List<Order> getUnRegisteredOrders(){
        List<Order> result =new ArrayList<>();
        if (orders() != null){
            try {
                result= orders().getUnRegistered();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<Item> getItems(){
        List<Item> result =new ArrayList<>();
        if (items() != null){
            try {
                result= items().getAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Item getItemByCode(Integer code){
        Item result =null;
        if (items() != null){
            try {
                List<Item> byCode = items().findIsNotCategoryByCode(code);
                if (byCode.size()!=1){
                    Log.i(TAG,"More then one item by code "+ code);
                    for(Item item:byCode){
                        Log.d(TAG, "Item: " + item.toString());
                    }
                }else if(byCode.size()==0){
                    Log.i(TAG,"Item not found by code "+ code);
                }else{
                    result=byCode.get(0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Item getCategoryByCode(Integer code){
        Item result =null;
        if (items() != null){
            try {
                List<Item> byCode = items().findIsCategoryByCode(code);
                if (byCode.size()!=1){
                    Log.i(TAG,"More then one item by code "+ code);
                    for(Item item:byCode){
                        Log.d(TAG, "Category: " + item.toString());
                    }
                }else if(byCode.size()==0){
                    Log.i(TAG,"Category not found by code "+ code);
                }else{
                    result=byCode.get(0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static List<Client> getClients(){
        List<Client> result =new ArrayList<>();
        if (clients() != null){
            try {
                result= clients().getAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Client getClientByCode(Integer code){
        Client result =null;
        if (clients() != null){
            try {
                List<Client> byCode = clients().findIsNotCategoryByCode(code);
                if (byCode.size()> 1){
                    Log.i(TAG,"More then one client by code "+ code);
                    for(Client client:byCode){
                        Log.d(TAG,"Client: "+client.toString());
                    }
                }else if(byCode.size()==0){
                    Log.i(TAG,"Client not found by code "+ code);
                } else{
                    result=byCode.get(0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Client getClientGroupByCode(Integer code){
        Client result =null;
        if (clients() != null){
            try {
                List<Client> byCode = clients().findIsCategoryByCode(code);
                if (byCode.size()!= 1){
                    Log.i(TAG,"More then one client by code "+ code);
                    for(Client client:byCode){
                        Log.d(TAG,"Client: "+client.toString());
                    }
                }else if(byCode.size()==0){
                    Log.i(TAG,"Client not found by code "+ code);
                }else{
                    result=byCode.get(0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static void clean(){
       getHelper().clean();
    }

}
