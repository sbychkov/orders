package bychkov.sergey.orders.service.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import bychkov.sergey.orders.event.SyncEvent;
import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.service.dbOps;
import bychkov.sergey.orders.util.EventBus;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by serge on 24.02.2015.
 */

public class Updates {
    //private static  final String SERVER_URL="http://172.16.1.22:8080/";
    private static final String SERVER_URL = "http://tom-sbychkov.rhcloud.com/ordersServer/";
    private static final String TAG = Updates.class.getCanonicalName();
    private Context mContext;
    private RestAdapter restAdapter;
    private Date lastSync = new Date(0L);
    private CtrlApi mApi;
    private Bus bus = EventBus.getInstance();

    public Updates() {
/*
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(90, TimeUnit.SECONDS);
        restAdapter = new RestAdapter.Builder().setClient(new OkClient(client)).setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(SERVER_URL).build();
        api=restAdapter.create(CtrlApi.class);
*/
    }


    public Updates(Date lastSync) {
        this.lastSync = lastSync;
    }

    public Updates(Context context) {
        mContext = context;
    }

    public Updates(Date lastSync,Context mContext ) {
        this.mContext = mContext;
        this.lastSync = lastSync;
    }

    public RestAdapter getAdapter() {
        if (restAdapter == null) {
            OkHttpClient client = new OkHttpClient();
            GsonBuilder gsonBuilder = new GsonBuilder();

           /* gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                @Override
                public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                        throws JsonParseException {
                    try {
                        return df.parse(json.getAsString());
                    } catch (ParseException e) {
                        return null;
                    }
                }
            });*/
            gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            Gson gson = gsonBuilder.create();


            RequestInterceptor requestInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    if (mContext != null) {
                        AccountManager accountManager =
                                (AccountManager) mContext.getSystemService(Context.ACCOUNT_SERVICE);
                        Account[] accountsByType = accountManager
                                .getAccountsByType("bychkov.sergey.orders.account");
                        if (accountsByType.length > 0) {
                            String username = accountsByType[0].name;
                            String password = accountManager.getPassword(accountsByType[0]);



                        }
                    }
                    final String credentials = "test:1q2w3e4r";
                    String string = "Basic " + Base64.encodeToString(credentials.getBytes()
                            , Base64.NO_WRAP);
                    request.addHeader("Authorization", string);
                    request.addHeader("User-Agent", "Orders Test Client");
                    request.addHeader("Accept", "application/json");
                }
            };


            client.setReadTimeout(90, TimeUnit.SECONDS);
            restAdapter = new RestAdapter.Builder()
                    .setClient(new OkClient(client))
                    .setConverter(new GsonConverter(gson))
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                            //.setErrorHandler(new myErrorHandler())
                    .setRequestInterceptor(requestInterceptor)
                    .setEndpoint(SERVER_URL)
                    .build();
        }
        return restAdapter;
    }

    public CtrlApi getApi() {
        if (mApi == null) {
            mApi = getAdapter().create(CtrlApi.class);
        }
        return mApi;
    }


    public void updateOrders() {
       /* for(Order order:dbOps.getUnRegisteredOrders()) {
            getApi().addOrder(order);
        }*/
        /*
        //
        for(Order order: getApi().getOrderList()){

        }
        */
        //TODO Update status of order
        for (Order order : dbOps.getUnRegisteredOrders()) {
            Order order1 = getApi().putOrder(order.toMap());
            if (order1 != null) {
                order.setRegId(order1.getId());
                try {
                    dbOps.orders().update(order);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateClients() {

        for (Client client : getApi().getClientLastChanges(String.valueOf(lastSync.getTime()))) {
            if (client != null) {
                Client dbClient = dbOps.getClientByCode(client.getCode());
                if (dbClient == null) {
                    // :( Amount of clients is too few or too much
                    //TODO Rise flag to check db or to create new entry

                } else {
                    // Renew client
                    if (client.getName() != null) {
                        if (!client.getName().isEmpty() &
                                !client.getName().equals(dbClient.getName()))
                            dbClient.setName(client.getName());
                    }
                    if (client.getParent() != null) {
                        if ((dbClient.getParent() == null ||
                                !client.getParent().getCode().equals(dbClient.getParent().getCode())))
                            dbClient.setParent(dbOps.getClientGroupByCode(client.getParent().getCode()));
                    }
                }
                try {
                    dbOps.clients().update(client);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void updateItems() {

        bus.post(new SyncEvent(SyncEvent.Type.Start));
        Collection<Item> items = getApi().getItemLastChanges(String.valueOf(lastSync.getTime()));
        bus.post(new SyncEvent(SyncEvent.Type.Stop));
        bus.post(new SyncEvent(SyncEvent.Type.MaxValue, items.size()));
        int counter = 0;
        for (Item item : items) {
            counter++;
            if (counter % 100 == 0) bus.post(new SyncEvent(SyncEvent.Type.IncValue, 100));
            if (item != null) {
                Item dbItem = dbOps.getItemByCode(item.getCode());
                if (dbItem == null) {
                    //TODO Rise flag to check db or to create new entry

                } else {
                    // Renew item
                    if (item.getName() != null) {
                        if (!item.getName().isEmpty() &
                                !item.getName().equals(dbItem.getName()))
                            dbItem.setName(item.getName());
                    }
                    if (item.getParent() != null) {
                        if ((dbItem.getParent() == null ||
                                !item.getParent().getCode().equals(dbItem.getParent().getCode())))
                            dbItem.setParent(dbOps.getCategoryByCode(item.getParent().getCode()));
                    }
                    if (item.getPrice() != null) {
                        if (item.getPrice().equals(dbItem.getPrice())
                                )
                            dbItem.setPrice(item.getPrice());
                    }

                }
                try {
                    dbOps.items().update(item);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        bus.post(new SyncEvent(SyncEvent.Type.Stop));
    }

    public void updatePrices() {

        bus.post(new SyncEvent(SyncEvent.Type.Start));
        Collection<Item> items = getApi().getItemLastChanges(String.valueOf(lastSync.getTime()));
        bus.post(new SyncEvent(SyncEvent.Type.Stop));
        bus.post(new SyncEvent(SyncEvent.Type.MaxValue, items.size()));
        int counter = 0;
        for (Item item : items) {
            counter++;
            if (counter % 100 == 0) bus.post(new SyncEvent(SyncEvent.Type.IncValue, 100));
            if (item != null) {
                Item dbItem = dbOps.getItemByCode(item.getCode());
                if (dbItem == null) {
                    //TODO Rise flag to check db or to create new entry

                } else {
                    // Renew item
                    if (item.getName() != null) {
                        if (!item.getName().isEmpty() &
                                !item.getName().equals(dbItem.getName()))
                            dbItem.setName(item.getName());
                    }
                    if (item.getParent() != null) {
                        if (dbItem.getParent() == null ||
                                !item.getParent().getCode().equals(dbItem.getParent().getCode()))
                            dbItem.setParent(dbOps.getCategoryByCode(item.getParent().getCode()));
                    }
                    if (item.getPrice() != null) {
                        if (!item.getPrice().equals(dbItem.getPrice()))
                            dbItem.setPrice(item.getPrice());
                    }
                }
                try {
                    dbOps.items().update(item);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        bus.post(new SyncEvent(SyncEvent.Type.Stop));
    }

    public void fetchClients() {
        //ArrayList<Client> roots= new ArrayList<>();
        // Brut method :)

        Collection<Client> clients = getApi().getClientList();
        for (Client client : clients) {
            if (client != null) {
                if (client.isCategory()) {
                    if (client.getParent() != null) {
                        Client parent = dbOps.getClientGroupByCode(client.getParent().getCode());
                        client.setParent(parent);
                    }
                    try {
                        dbOps.clients().create(client);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        for (Client client : clients) {
            if (client != null) {
                if (!client.isCategory()) {
                    if (client.getParent() != null) {
                        Client parent = dbOps.getClientGroupByCode(client.getParent().getCode());
                        client.setParent(parent);
                    }
                    try {
                        dbOps.clients().create(client);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        //TODO Add findClientByParentMethod


    }

    public void fetchItems() {


        Collection<Item> items = getApi().getCategoryList();
        for (Item item : items) {
            if (item != null) {
                if (item.getParent() != null) {
                    Item parent = dbOps.getCategoryByCode(item.getParent().getCode());
                    item.setParent(parent);
                }
                try {
                    item.setCategory(true);
                    dbOps.items().create(item);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        items = getApi().getItemList();
        for (Item item : items) {
            if (item != null) {
                if (item.getParent() != null) {
                    Item parent = dbOps.getCategoryByCode(item.getParent().getCode());
                    item.setParent(parent);
                }
                try {
                    item.setCategory(false);
                    dbOps.items().create(item);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    public void login() {


    }


    private class myErrorHandler implements ErrorHandler {


        @Override
        public Throwable handleError(RetrofitError cause) {
            Log.w(TAG, cause.getUrl());
            Log.w(TAG, String.valueOf(cause.getResponse()));
            Log.w(TAG, cause.getResponse().getReason());
            Log.w(TAG, String.valueOf(cause.getResponse().getStatus()));
            return cause;
        }
    }

}
