package bychkov.sergey.orders.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.service.sync.Api;
import bychkov.sergey.orders.service.sync.CtrlApi;
import bychkov.sergey.orders.util.HelperFactory;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;

/**
 * Created by serge on 23.12.2014.
 */
public class Sync extends AsyncTask {

    private static final String TAG = Sync.class.getSimpleName();
    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    private int i;
    private Context mContext;
    private RestAdapter restAdapter;

    public Sync(Context context) {
        mContext = context;
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(90, TimeUnit.SECONDS);
        restAdapter = new RestAdapter.Builder().setClient(new OkClient(client)).setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(mContext.getString(R.string.serverUrl)).build();

    }

    @Override
    protected Object doInBackground(Object[] params) {
        if (params != null && params.length > 0) {

            if (params[0] == "update") {
                update();
            }
            if (params[0] == "clean") {
                clean();
            }
            if (params[0] == "updatePrices") {
                prices();
            }
        } else {
            full();
        }
        return i;
    }

    private void full() {

        Log.i(TAG, "Full sync");
        Log.i(TAG, "Connecting to server");
        try {
            clean();
            clients();
            items();
            pricesAll();
            orders();
        } catch (RetrofitError re) {
            failure(re);
        }

    }


    private void update() {
        try {
            Log.i(TAG, "Check updates");
            Log.i(TAG, "Connecting to server");

            //restAdapter = new RestAdapter.Builder().setEndpoint(mContext.getString(R.string.serverUrl)).build();

            Api api = restAdapter.create(Api.class);
            Log.i(TAG, "connected");
            Log.i(TAG, "Fetching clients");

            Log.i(TAG, "Fetching items");

            Log.i(TAG, "Fetching prices");

            Log.i(TAG, "Sending orders");
            /*try {
                List<Order> lo = HelperFactory.getHelper().getOrderDao().getUnRegistered();
                for (Order o : lo) {

                    Long id = Long.parseLong(api.sendOrder(o));
                    if (id != null) {
                        o.setRegId(id);
                        HelperFactory.getHelper().getOrderDao().update(o);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }*/
            orders();
        } catch (RetrofitError re) {
            if (re.getCause().getClass().equals(SocketTimeoutException.class)) {
                Log.i(TAG, "Connection timeout " + re.getUrl());
            } else if (re.getLocalizedMessage() != null) {
                Log.i(TAG, re.getLocalizedMessage());
            } else {
                Log.i(TAG, re.toString());
            }
        }
    }


    private void clients() {
        Api api = restAdapter.create(Api.class);
        Log.i(TAG, "connected");
        Log.i(TAG, "Fetching clients");
        List<Client> clientList = api.getClients();
        i = clientList.size();
        Log.i(TAG, clientList.size() + " clients received");
        try {
            for (Client c : clientList) {
                Client parent = null;
                if (c.getParent() != null) {
                    List<Client> cl = HelperFactory.getHelper().getClientDao()
                            .getByCode(c.getParent().getCode());
                    if (cl != null && !cl.isEmpty()) {
                        if (cl.size() > 1) {
                            Log.w(TAG, "More than one entry for code " + c.getParent().getCode());
                        }
                        parent = cl.get(0);
                    }
                }
                List<Client> cl = HelperFactory.getHelper().getClientDao().getByCode(c.getCode());
                if (cl != null && !cl.isEmpty()) {
                    if (cl.size() > 1) {
                        Log.w(TAG, "More than one entry for code " + c.getCode());
                    } else {
                        HelperFactory.getHelper().getClientDao().update(c);
                    }
                } else {
                    c = HelperFactory.getHelper().getClientDao().createIfNotExists(c);

                }
                c.setParent(parent);
                HelperFactory.getHelper().getClientDao().update(c);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void items() {
        Api api = restAdapter.create(Api.class);
        Log.i(TAG, "Fetching items");
        List<Item> itemList = api.getItems();
        Log.i(TAG, itemList.size() + " items received");
        i += itemList.size();
        try {
            for (Item item : itemList) {
                Item parent = null;
                if (item.getParent() != null) {
                    List<Item> il = HelperFactory.getHelper().getItemDao().findIsCategoryByCode(item.getParent().getCode());

                    if (il != null && !il.isEmpty()) {
                        if (il.size() > 1) {
                            Log.w(TAG, "More than one entry for code " + item.getParent().getCode());
                        }
                        parent = il.get(0);
                    }
                }
                item = HelperFactory.getHelper().getItemDao().createIfNotExists(item);
                item.setParent(parent);
                HelperFactory.getHelper().getItemDao().update(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void pricesAll() {
        Log.i(TAG, "Fetching prices");
        Api api = restAdapter.create(Api.class);
        List<Item> pricesList = api.getPrice();
        Log.i(TAG, "Prices for " + pricesList.size() + " items received");
        try {
            for (Item item : pricesList) {
                List<Item> il = HelperFactory.getHelper().getItemDao().findIsNotCategoryByCode(item.getCode());
                if (il != null && !il.isEmpty()) {
                    if (il.size() > 1) {
                        Log.w(TAG, "More than one entry for code " + item.getParent().getCode());
                    }
                    Item itemDb = il.get(0);
                    itemDb.setPrice(item.getPrice());
                    itemDb.setQuantity(item.getQuantity());
                    HelperFactory.getHelper().getItemDao().update(itemDb);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void prices() {
        Log.i(TAG, "Fetching prices");
        try {
            Api api = restAdapter.create(Api.class);
            try {
                List<Item> nodes = HelperFactory.getHelper().getItemDao().getByParent(null);
                while (!nodes.isEmpty()) {
                    Item item = nodes.remove(0);
                    if (item.isCategory()) {
                        nodes.addAll(HelperFactory.getHelper().getItemDao().getByParent(item));

                        for (Item item2 : api.getPrice(item.getCode().toString())) {
                            List<Item> pl = HelperFactory.getHelper().getItemDao().findIsNotCategoryByCode(item2.getCode());
                            if (pl != null && pl.size() > 0) {
                                if (pl.size() > 1) {
                                    Log.w(TAG, "More than one entry for code " + item2.getCode());
                                }
                                if (item2.getPrice() != null && item2.getQuantity() != pl.get(0).getQuantity()) {
                                    pl.get(0).setPrice(item2.getPrice());
                                    pl.get(0).setQuantity(item2.getQuantity());
                                    HelperFactory.getHelper().getItemDao().update(pl.get(0));
                                }
                            }

                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (RetrofitError re) {
            failure(re);
        }


    }


    private void orders() {
        Log.i(TAG, "Sending orders");
        //Api api = restAdapter.create(Api.class);
        CtrlApi api = restAdapter.create(CtrlApi.class);
        try {
            /*List<Order> orderList = HelperFactory.getHelper().getOrderDao().getUnRegistered();
            for (Order o : orderList) {
                Order dto = new Order(o.getCreateDate(),o.getSum(),o.getElements());
                dto.setClient(o.getClient());

                Long id = Long.parseLong(api.sendOrder(dto));
                if (id != null) {
                    o.setRegId(id);
                    HelperFactory.getHelper().getOrderDao().update(o);
                }
            }
            */
            for (Order order:dbOps.orders().getAll()){
                api.putOrder(order.toMap());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Finished");
    }


    private void failure(RetrofitError re) {
        if (re.getCause()!=null&&re.getCause().getClass().equals(SocketTimeoutException.class)) {
            Log.i(TAG, "Connection timeout " + re.getUrl());

        } else if (re.getLocalizedMessage() != null) {
            Log.i(TAG, re.getLocalizedMessage());
        } else {
            Log.i(TAG,re.getBody().toString());
            Log.i(TAG, re.toString());
        }
    }

    private void clean() {
        HelperFactory.getHelper().clean();
    }

    protected void OnProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }
    // This is executed in the context of the main GUI thread

    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param o The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Toast toast = Toast.makeText(mContext, " " + i, Toast.LENGTH_SHORT);
        toast.show();
    }
}
