package bychkov.sergey.orders.service.auth;

/**
 * Created by serge on 03.04.2015.
 */

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import bychkov.sergey.orders.R;

public class AuthTokenLoader extends AsyncTaskLoader<String> {

    private final String mObtainTokenUrl;

    private final String mLogin;

    private final String mPassword;

    private String mAuthToken;

    public AuthTokenLoader(Context context, String login, String password) {
        super(context);
        mObtainTokenUrl = context.getString(R.string.serverUrl);
        mLogin = login;
        mPassword = password;
    }

    public static String signIn(Context context, String login, String password) {
        try {
            return new AuthTokenLoader(context, login, password).signIn();
        } catch (IOException e) {
            Log.e(AuthTokenLoader.class.getSimpleName(), e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onStartLoading() {
        if (TextUtils.isEmpty(mAuthToken)) {
            forceLoad();
        } else {
            deliverResult(mAuthToken);
        }
    }

    @Override
    public void deliverResult(String data) {
        mAuthToken = data;
        super.deliverResult(data);
    }

    @Override
    public String loadInBackground() {
        try {
            return signIn();
        } catch (IOException e) {
            Log.e(AuthTokenLoader.class.getSimpleName(), e.getMessage(), e);
        }
        return null;
    }

    private String signIn() throws IOException {
        final HttpURLConnection cn = (HttpURLConnection) new URL(mObtainTokenUrl).openConnection();
        cn.setRequestMethod("PUT");
        cn.addRequestProperty("Accept", "application/json");
        cn.addRequestProperty("Authorization", "Basic " +
                Base64.encodeToString((mLogin + ":" + mPassword).getBytes(), Base64.DEFAULT));
        sendBody(cn);
        return readToken(cn);
    }

    private void sendBody(HttpURLConnection cn) throws IOException {

    }

    private String readToken(HttpURLConnection cn) throws IOException {

        return null;
    }

}