package bychkov.sergey.orders.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.model.Client;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.model.Order;
import bychkov.sergey.orders.model.OrderElement;
import bychkov.sergey.orders.util.Adapter.OrderItemsAdapter;
import bychkov.sergey.orders.util.HelperFactory;

public class OrderActivity extends ActionBarActivity {

private static final String TAG = OrderActivity.class.getSimpleName();

    private static final int ADD_ITEM_REQUEST = 0;
    private static final int SELECT_CLIENT_REQUEST = 1;
    private Button clientButton;
    private ListView listView;
    private OrderItemsAdapter mAdapter;
    private Client client;
    private TextView totalText;
    private Order order=new Order();
    private    BigDecimal total = BigDecimal.ZERO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        clientButton = (Button) findViewById(R.id.clientButton);
        clientButton.setText("Клиент");
        clientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderActivity.this, ClientActivity.class);
                startActivityForResult(intent, SELECT_CLIENT_REQUEST);
            }
        });

        listView = (ListView) findViewById(R.id.items);

        listView.setFooterDividersEnabled(true);
        TextView footerView = (TextView) getLayoutInflater().inflate(R.layout.footer_item, null);
        listView.addFooterView(footerView);
        mAdapter = new OrderItemsAdapter(this);
        listView.setAdapter(mAdapter);

        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrderActivity.this, AddItemActivity.class);
                startActivityForResult(i, ADD_ITEM_REQUEST);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = mAdapter.getItem(position);
                if (item != null) {
                    mAdapter.remove(position);
                    return true;
                } else {
                    return false;
                }
            }
        });



        Button button = (Button) findViewById(R.id.okButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.setCreateDate(new Date());
                order.setClient(client);
                order.setElements(mAdapter.getAll());
                order.setSum(total);
                try {
                    HelperFactory.getHelper().getOrderDao().create(order);
                    for(OrderElement el : order.getElements()){
                        HelperFactory.getHelper().getOrderElementDao().create(el);
                    }

                    Log.i(TAG,"Order " + order + " persisted");
                }catch (SQLException e){
                    e.printStackTrace();
                }
                OrderActivity.this.finish();
            }
        });

        totalText= (TextView) findViewById(R.id.total_sum);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_ITEM_REQUEST && resultCode == RESULT_OK) {
            OrderElement oe = new OrderElement();
            oe.setItem((Item) data.getSerializableExtra("item"));
            oe.setPrice((BigDecimal) data.getSerializableExtra("price"));
            oe.setQuantity((Integer) data.getSerializableExtra("count"));
            oe.setOrder(order);
            mAdapter.add(oe);
            updateTotal();
        }
        if (requestCode == SELECT_CLIENT_REQUEST && resultCode == RESULT_OK){
         client = (Client) data.getSerializableExtra("client");
            if(client!=null&&client.getName()!=null){
            clientButton.setText(client.getName());
        }
        }
    }

    private void updateTotal(){
     total = BigDecimal.ZERO;
        for(OrderElement oe: mAdapter.getAll()){
          total=total.add(oe.getPrice().multiply(BigDecimal.valueOf(oe.getQuantity())));
       }
        totalText.setText(total.toPlainString() + " руб.");
    }
}
