package bychkov.sergey.orders.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.event.SyncEvent;
import bychkov.sergey.orders.service.dbOps;
import bychkov.sergey.orders.service.sync.SyncUtils;
import bychkov.sergey.orders.util.EventBus;


public class SyncActivity extends ActionBarActivity {

    private    Bus bus = EventBus.getInstance();
    private ProgressBar totalPB;
    private  ProgressBar syncPB;
    private TextView total;
    private TextView title;
    private TextView current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        Button totalSync = (Button) findViewById(R.id.totalSyncButton);
        totalSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncUtils.fullDump();
            }
        });

        Button clean = (Button) findViewById(R.id.cleanDbButton);
        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbOps.clean();
            }
        });

        Button update = (Button) findViewById(R.id.updateButton);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncUtils.triggerRefresh();
            }
        });

        Button updatePrices = (Button) findViewById(R.id.updatePricesButton);
        updatePrices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncUtils.pricesOnly();
            }
        });


        totalPB = (ProgressBar) findViewById(R.id.progressBarTotal);

        syncPB = (ProgressBar) findViewById(R.id.progressBarSync);

        total = (TextView) findViewById(R.id.total);
        current = (TextView) findViewById(R.id.current);
        title = (TextView) findViewById(R.id.title);

        bus.register(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sync, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void getSyncEvent(SyncEvent event){

        switch (event.getType()){

            case Start:
                syncPB.setProgress(0);
                totalPB.setProgress(0);
                syncPB.setVisibility(View.VISIBLE);
                break;
            case Stop:
                syncPB.setProgress(0);
                totalPB.setProgress(0);
                syncPB.setVisibility(View.GONE);
                break;
            case MaxValue:
                if (syncPB.getVisibility()!=View.VISIBLE){
                    syncPB.setProgress(0);
                    syncPB.setVisibility(View.VISIBLE);
                }
                current.setText("0");
                total.setText(event.getValue());
                title.setText(event.getTitle());
                syncPB.setMax(event.getValue());
                break;
            case MaxTotalValue:
                if (totalPB.getVisibility() != View.VISIBLE){
                    totalPB.setProgress(0);
                    totalPB.setVisibility(View.VISIBLE);
                }
                totalPB.setMax(event.getValue());
                break;
            case IncTotal:
                totalPB.incrementProgressBy(1);
                break;
            case IncValue:

                current.setText(str2int(current.getText().toString()) + event.getValue());
                syncPB.incrementProgressBy(event.getValue());
                break;
            case Inc:
                current.setText(str2int(current.getText().toString())+ 1);
                syncPB.incrementProgressBy(1);
                break;
            case Title:
                title.setText(event.getTitle());
                break;
            case TotalTitle:
                break;
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    private Integer str2int(String str) {
        Integer result = 0;
        if (str != null && !str.trim().isEmpty()) {
            result = Integer.parseInt(str.replace(",", "."));
        }
        return result;
    }

}
