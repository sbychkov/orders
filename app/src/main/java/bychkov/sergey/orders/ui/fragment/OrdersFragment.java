package bychkov.sergey.orders.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import bychkov.sergey.orders.R;
import bychkov.sergey.orders.ui.activity.OrderActivity;
import bychkov.sergey.orders.util.Adapter.OrdersAdapter;


/**
 * A placeholder fragment containing a simple view.
 */
public class OrdersFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    //private static final String ARG_SECTION_NUMBER = "section_number";
    private Button button;
    private ListView listView;
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static OrdersFragment newInstance(int sectionNumber) {
        OrdersFragment fragment = new OrdersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public static OrdersFragment newInstance(){
        return new OrdersFragment();
    }

    public OrdersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Context context = getActivity();
        View rootView = inflater.inflate(R.layout.fragment_orders, container, false);


        button = (Button) rootView.findViewById(R.id.newOrderButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrderActivity.class);
                getActivity().startActivity(intent);
            }
        });
        listView = (ListView) rootView.findViewById(R.id.ordersListView);
        listView.setAdapter(new OrdersAdapter(this.getActivity()));
        listView.setFooterDividersEnabled(true);
        TextView footerView= (TextView)   getLayoutInflater(savedInstanceState).inflate(R.layout.footer_orders, null);
        listView.addFooterView(footerView);

        return rootView;
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();
    }
}
