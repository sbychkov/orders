package bychkov.sergey.orders.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import bychkov.sergey.orders.event.BackEvent;
import bychkov.sergey.orders.model.Item;
import bychkov.sergey.orders.util.Adapter.PriceListAdapter;
import bychkov.sergey.orders.util.EventBus;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 *
 */
public class PriceListFragment extends ListFragment {

    private String parent;
    private Bus bus = EventBus.getInstance();
    private PriceListAdapter priceListAdapter;

    public static PriceListFragment newInstance() {
        PriceListFragment fragment = new PriceListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PriceListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
       //     parent= getArguments().getString(ARG_PARENT);
                    }

        priceListAdapter = new PriceListAdapter(getActivity(), null);
        setListAdapter(priceListAdapter);

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        ((PriceListAdapter) getListAdapter()).setParent((Item) getListAdapter().getItem(position));


    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    /**
     * Called when the Fragment is no longer resumed.  This is generally
     * tied to {@link Activity#onPause() Activity.onPause} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Subscribe
    public void backButton(BackEvent backEvent){
        if(backEvent.getType().equals(BackEvent.Type.backButton)){
            if(priceListAdapter.back()& this.isVisible()){
                bus.post(new BackEvent(BackEvent.Type.exitApp));
            }
        }
    }

}
